module gitlab.com/projector-highload/aws-lambda

go 1.17

require (
	github.com/aws/aws-lambda-go v1.27.0
	github.com/aws/aws-sdk-go-v2/config v1.9.0
	github.com/aws/aws-sdk-go-v2/service/s3 v1.17.0
	github.com/sunshineplan/imgconv v1.0.2
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)

require (
	github.com/aws/aws-sdk-go-v2 v1.10.0 // indirect
	github.com/aws/aws-sdk-go-v2/credentials v1.5.0 // indirect
	github.com/aws/aws-sdk-go-v2/feature/ec2/imds v1.7.0 // indirect
	github.com/aws/aws-sdk-go-v2/internal/ini v1.2.5 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/accept-encoding v1.4.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/presigned-url v1.4.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/s3shared v1.8.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/sso v1.5.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/sts v1.8.0 // indirect
	github.com/aws/smithy-go v1.8.1 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/hhrutter/lzw v0.0.0-20190829144645-6f07a24e8650 // indirect
	github.com/hhrutter/tiff v0.0.0-20190829141212-736cae8d0bc7 // indirect
	github.com/pdfcpu/pdfcpu v0.3.12 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sunshineplan/pdf v1.0.0 // indirect
	github.com/sunshineplan/tiff v0.0.0-20210710125142-3366bd152a12 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
