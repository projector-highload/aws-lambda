package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"image"
	"log"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	runtime "github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/sunshineplan/imgconv"
	"golang.org/x/sync/errgroup"
)

var targetFormats = []string{"png", "bmp", "gif"}

func callLambda(ctx context.Context, entity events.S3Entity) error {
	cfg, err := config.LoadDefaultConfig(ctx, config.WithDefaultRegion("eu-central-1"))
	if err != nil {
		log.Fatalf("failed to load SDK configuration, %v", err)
	}

	client := s3.NewFromConfig(cfg)

	img, err := fetchImage(ctx, client, entity)
	if err != nil {
		return err
	}

	errg, ctx := errgroup.WithContext(ctx)

	for _, format := range targetFormats {
		format := format

		errg.Go(func() error {
			return processImage(ctx, client, img, entity.Object.Key, format, entity.Bucket.Name)
		})
	}

	return errg.Wait()
}

func processImage(
	ctx context.Context,
	client *s3.Client,
	img image.Image,
	imgName string,
	targetFormat string,
	targetBucket string,
) error {
	opts := imgconv.NewOptions()
	if err := opts.SetFormat(targetFormat); err != nil {
		return err
	}

	buff := make([]byte, 0)
	newImg := bytes.NewBuffer(buff)

	if err := opts.Convert(newImg, img); err != nil {
		return err
	}

	newKey := strings.Replace(imgName, ".jpg", fmt.Sprintf(".%s", targetFormat), 1)

	putInput := &s3.PutObjectInput{Bucket: &targetBucket, Key: &newKey, Body: newImg}
	_, err := client.PutObject(ctx, putInput)

	return err
}

func fetchImage(ctx context.Context, client *s3.Client, entity events.S3Entity) (image.Image, error) {
	input := &s3.GetObjectInput{Bucket: &entity.Bucket.Name, Key: &entity.Object.Key}

	output, err := client.GetObject(ctx, input)
	if err != nil {
		return nil, err
	}

	return imgconv.Decode(output.Body)
}

func handleRequest(ctx context.Context, event events.S3Event) (string, error) {
	// event
	eventJson, _ := json.Marshal(event)
	log.Printf("EVENT: %s", eventJson)
	// environment variables
	log.Printf("REGION: %s", os.Getenv("AWS_REGION"))
	log.Println("ALL ENV VARS:")
	for _, element := range os.Environ() {
		log.Println(element)
	}
	// request context
	lc, _ := lambdacontext.FromContext(ctx)
	log.Printf("REQUEST ID: %s", lc.AwsRequestID)
	// global variable
	log.Printf("FUNCTION NAME: %s", lambdacontext.FunctionName)
	// context method
	deadline, _ := ctx.Deadline()
	log.Printf("DEADLINE: %s", deadline)
	// AWS SDK call
	err := callLambda(ctx, event.Records[0].S3)
	if err != nil {
		return "ERROR", err
	}

	return "SUCCESS", nil
}

func main() {
	runtime.Start(handleRequest)
}
